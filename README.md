Scripts to bootstrap host tool environments for BuildStream.

Dependencies required on the host include:

 * GNU C and C++ toolchain
 * patch
 * wget
