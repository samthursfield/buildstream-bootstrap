#!/bin/bash

set -ex

# Bootstrap using musl-cross-make and mkroot.

ARCH=$1
case $ARCH in
  armv8b64)
    TARGET=aarch64_be-linux-musl ;;
  armv8l64)
    TARGET=aarch64-linux-musl ;;
  ppc64l) 
    TARGET=powerpc64le-linux-musl ;;
  ppc64b)
    TARGET=powerpc64-linux-musl ;;
  *)
    echo >&2 "Invalid target specified: $1. Please specify one of the "
    echo >&2 "supported architecture names as the first parameter."
    exit 1
esac
  

# 1. Build a cross compiler for $TARGET using musl-cross-make.

cd musl-cross-make

cat > ./config.mak <<STAGE1_CONFIG_EOF
COMMON_CONFIG += --disable-nls
GCC_CONFIG += --enable-languages=c,c++
GCC_CONFIG += --disable-libquadmath --disable-decimal-float
GCC_CONFIG += --disable-multilib
STAGE1_CONFIG_EOF

gmake TARGET=$TARGET

gmake TARGET=$TARGET OUTPUT=`pwd`/output-stage1-$ARCH install

STAGE1_COMPILER_BIN=`pwd`/output-stage1-$ARCH/bin

# 2. Build a native compiler for $TARGET using musl-cross-make

cat > config.mak <<STAGE2_CONFIG_EOF
COMMON_CONFIG += CC="$TARGET-gcc -static --static" CXX="$TARGET-g++ -static --static"
COMMON_CONFIG += --disable-nls
GCC_CONFIG += --enable-languages=c,c++
GCC_CONFIG += --disable-libquadmath --disable-decimal-float
GCC_CONFIG += --disable-multilib
STAGE2_CONFIG_EOF

PATH=$STAGE1_COMPILER_BIN:$PATH gmake NATIVE=yes TARGET=$TARGET
PATH=$STAGE1_COMPILER_BIN:$PATH gmake NATIVE=yes TARGET=$TARGET OUTPUT=`pwd`/output-stage2-$ARCH install


# 3. Build a rootfs using mkroot

cd ../mkroot

# Work around what appears to be a mkroot bug
mkdir -p airlock
ln -sf /usr/bin/bc ./airlock/bc

PATH=$STAGE1_COMPILER_BIN:$PATH CROSS_COMPILE=$TARGET- \
    ./mkroot.sh kernel

echo "Output in mkroot/output directory"
